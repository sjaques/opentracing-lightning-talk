# See README.md for details

TARGETS = slides.html slides-offline.html slides.pdf images/jsonopentracing.png

# enable verbose output with 'make V=1'
ifndef V
    Q = @
endif

all: $(TARGETS)
	$(Q)rm -rf template/base.html
	@echo Done.

define insertfile # (marker, file, input)
	sed -e '/$1/r $2' -e '/$1/d' $3 > $3.tmp 2>/dev/null ; \
	mv $3.tmp $3
endef

define replacetext # (marker, replacement, inputoutput)
	sed -e 's/$1/$2/g' $3 > $3.tmp 2>/dev/null ; \
	mv $3.tmp $3
endef

define extracttext # (prepattern, postpattern, input)
$(shell sed -n '\%$1.*$2%{s%$1\(.*\)$2%\1%p}' slides.md 2>/dev/null)
endef

FONTSMARKER = <!--FONTS-MARKER-->
SLIDEMARKER = <!--SLIDES-MARKER-->
SUPPORTMARKER = <!--SUPPORT-MARKER-->
TITLEMARKER = <!--TITLE-MARKER-->
YEARMARKER = <!--YEAR-MARKER-->
CLASSIFICATIONMARKER = <!--CLASSIFICATION-MARKER-->

TITLE := $(call extracttext,^\# TITLE = ,,slides.md)
YEAR := $(call extracttext,^\# YEAR = ,,slides.md)
CLASSIFICATION := $(call extracttext,^\# CLASSIFICATION = ,,slides.md)

slides.html: template/support.html.in template/base.html
slides-offline.html: template/support-offline.html.in template/base.html
slides.html slides-offline.html:
	$(Q)cp template/base.html $@
	$(Q)$(call insertfile,$(SUPPORTMARKER),$<,$@)

template/base.html: template/base.html.in slides.md
	$(Q)cp $< $@
	$(Q)$(call insertfile,$(SLIDEMARKER),slides.md,$@)
	$(Q)$(call insertfile,$(FONTSMARKER),template/fonts.css,$@)
	$(Q)$(call replacetext,$(TITLEMARKER),$(TITLE),$@)
	$(Q)$(call replacetext,$(YEARMARKER),$(YEAR),$@)
	$(Q)$(call replacetext,$(CLASSIFICATIONMARKER),$(CLASSIFICATION),$@)

clean:
	$(Q)rm -rf $(TARGETS) template/base.html

slides.md:
	@if [ ! -f $@ ]; then \
		echo "WARNING: $@ not found. An example has been created automatically."; \
		echo "Edit it with your slides content, then run make again."; \
		cp template/slides.md.example $@; \
	fi

slides.pdf: slides-offline.html images/jsonopentracing.png
	docker run --rm -v `pwd`:/slides  astefanutti/decktape:2.8 remark file:///slides/slides-offline.html slides.pdf

images/jsonopentracing.png:
	(cat isam-arch.uml | docker run --rm -i think/plantuml -tpng > tmp.png || (rm tmp.png; false) ) && mv tmp.png images/jsonopentracing.png
