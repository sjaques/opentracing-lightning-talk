# Nokia Markdown slide template

Write slides in Markdown syntax in a plain text file.  
Display them in Nokia template in a web browser.

Upstream URL: https://gitlabe1.ext.net.nokia.com/tdescham/nokia-slide-template-markdown

## Instructions

* create an initial `slides.md` file:
  ```
  make slides.md
  ```

* update the configuration section in `slides.md`

* populate `slides.md` with your slides.
  Refer to the [remarkjs GitHub wiki](https://github.com/gnab/remark/wiki) for details about formatting.

* generate html output
  ```
  make
  ```

* open either `slides.html` (requires an Internet connection) or
  `slides-offline.html` in a modern web browser (e.g. Firefox, Chrome)

## Suggested workflow

The suggested workflow while making your slides is as follows:

1. Clone this repository to a new location, specific for your current
   presentation
2. Start creating your slides and add `slides.md` under version control
3. Regularly commit your changes to `slides.md`
4. If needed, you can make custom changes to the support code, e.g. to the CSS
   and commit them too.

To line up your presentation with a newer version of this slide template, make
sure the latest changes have been committed, then use:
```
git pull --rebase
```
Alternatively, you can merge instead of rebase.

## How it works

The main component is [remarkjs](https://remarkjs.com/) which renders the
Markdown as slides using JavaScript.

## PDF export

Converting the slides to PDF format is possible via [DeckTape](https://github.com/astefanutti/decktape). Follow the instructions provided there.

In DeckTape 1.x, the process involved phantomjs.  
Starting from DeckTape 2.x, puppeteer is used. Using docker, you don't have to
install anything. Just run:

```
docker run --rm -v `pwd`:/slides  astefanutti/decktape:2.8 remark file:///slides/slides-offline.html slides.pdf
```

## Notes

* Trying to send the html files by email may cause corrupted files at the
  receiving side. The Exchange server seems to garble them.
  Quick solution is to wrap the files, e.g. in a .gz, .tar or .zip file, before
  sending.

## License

remark is licensed under the MIT license. See file LICENSE-remark.

The support code is equally licensed under the MIT license. See file LICENSE.
