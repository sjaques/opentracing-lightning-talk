class: titleslide middle

<!--
Configuration **********************************************************
# TITLE = OpenTracing
# YEAR = 2018
Possible values: Secret, Confidential, Nokia Internal Use, Public
# CLASSIFICATION = Public
************************************************************************
-->

.center[![opentracing](images/opentracing_logo.png)]
.center[
<i> Monitor transactions in distributed systems with vendor-neutral APIs </i>]
.center[![micro](images/microservice_trans.png)]
&nbsp;
.center[.smaller[<i> Some human beings believe in the fifth dimension</i>]]
.center[.smaller[<i>Others try to understand the sixth</i>]]
.center[.smaller[<i>We'll show you the way to the seventh dimension</i>]]

.right[Jan Heylen

29 May 2018
]

???

Opentracing: monitor transaction in distributed systems iwth vendor neutral APIs

A lot of word to say something simple actually

In Moswa, one of the key problems we want to solve is how to follow a packet or e.g. a netconf configuration request through the distributed system,

---

# Why we need distributed tracing

.center[![images/microservice.png](images/concurrency.png)]


???

A system in moswa can consist of many little application, or third party elements, even not on the box, that can play a role, in a concurrent asynchronous system.

So you get distributed concurrency via events that happen.

The basic principles of the solution to this problem,  are known, you assign each request a unique ID and pass it along over each boundary, so you can track it through your system. This idea is called 'distributed tracing' and that idea is not new.
---

# History

.center[![dapper](images/dapper.png)]

???

Besides academic papers about distributed tracing, 
and similar systems in distributed frameworks, like some of our own.

14 years ago, the amount of data coming into Goolge's services required distributed tracing at scale,

and Dapper was created in house at Google. 

8 years ago, they put the Dapper paper online, explaining distributed tracing at scale. 

A couple of initiatives started, and e.g. Zipkin became openZipkin. Ben Siegelman left google and started lightstep

---

# [context tracing in SNMP ISAM NT/LT](https://acos.alcatel-lucent.com/wiki/g/isamsw/Context%20tracing)

.center[![context tracing](images/nt-lt-tracing.png)]
&nbsp;
.center[![context tracing](images/context_tracing.png)]

???

And about a year ago, around the same time we where looking into the problem for MOSWA, and came in contact with OpenTracing, Ricardo showed us his context tracing system, that was using these basic principles, in an in house made solution, to track a request between an nt and an lt in an SNMP xapi based system.

Details of this effort can be found on the wiki.

---

# OpenTracing

.center[![opentracing diagram](images/opentracing_diagram.png)]

???
What is new, and what I'll be showing today is based upon this approach, is that the instrumentation is done using an open API, standardized, the Opentracing api, so you can instrument your code using a standard API (which we obviously shield by yAPI)

and interpret the result with available tools that assist you in visualising and analysing the data. This open API is build upon structured data, so no log parsing, by making use of standards like JSON/thrift/protobuf. Making use of structured data seems like a small step but it makes a huge difference in using and storing the data afterwards.

The nice part about leveraging on open source building blocks and open APIS, is that you 'intrument your code once' and can plugin different tracers and outputs, and use different tools to analyse the date, depending on the use case.

Besides tracing a request, I believe it will also be very useful in analysing performance, especially latency.

I will basically tell you what Ben Siegelman and the guys from Jaeger will also tell you, but I couldn't get them here, so here I am.

---

# Spans and Traces

.center[![spans](images/spans-traces.png)]

.center[https://github.com/opentracing/specification/blob/master/specification.md]

???

First some terminology.

A trace exists of one or multiple spans, a span contains the information about 'the operation performed', start/end time, tags and logs, references and a spancontext, used to propagate a span.

A span reference can be ATM: a 'childof' relateion or a 'followsfrom', details in the specification.

There is a bit more to understand all the details on how opentracing works, but let's go to the demo with just this piece of information. You can talk to me or Sebastien for the full picture.

---

# Propagation

.center[![approach](images/approach.png)]

---

# Jaeger architecture

.center[![jaeger](images/jaeger_architecture.png)]

---

# Opentracing in Moswa

.center[![moswa](images/jsonopentracing.png)]

---

class: titleslide center middle

# Demonstration of distributed tracing using OpenTracing

.imagerigth[![jaeger](images/jaeger.png)]

&nbsp;

.imageleft[![uber](images/uber_small.png)]
## Uber's HOT R.O.D. and Jaeger

## .remark-code[.xxl[http://nok.it/hotrod]]

---

# One last thing


http://opentracing.io/documentation/pages/translations

.center[![Lost-in-translation](images/lost-in-translation.png)]

???

One last thing, there is a Chinese translation of the opentracing specificaiton and documentation:

---
class: finalslide center

![nokia](images/opentracing_logo.png)
